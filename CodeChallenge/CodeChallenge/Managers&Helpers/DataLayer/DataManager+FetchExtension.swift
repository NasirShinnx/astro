import Foundation
extension DataManager {
    
    func fetchChannelsData(_ apiType: APIEndPointType, sortType:SortType, _ completion: @escaping (_ results: [ChannelModel]?, _ error: DataError?) -> Void){
    
        if let realm = RealmProvider.realm(){
            let channels = (realm.objects(ChannelModel.self).toArray())
            if !channels.isEmpty {
                completion(channels, nil)
            }
            else{
                FetchChannels() {(channels : [ChannelModel]) in
                    completion(channels, nil)
                    }.execute(apiType)
            }
        }
        else{
            print("error")
        }
    }
    
    //MARK: Event Data Provider
    func fetchEventsParamsForChannel(_ channel : ChannelModel, periodStart : Date, periodEnd: Date, _ completion: @escaping (_ results: [EventModel]?, _ error: DataError?) -> Void) {
       
        let periodStartParameter = periodStart.string(formatted: "YYYY-MM-dd HH:mm")
        let periodEndParameter =  periodEnd.string(formatted: "YYYY-MM-dd HH:mm")
        let params = ["channelId": channel.channelId.value!, "periodStart" : periodStartParameter!, "periodEnd": periodEndParameter!] as [String : Any]
        
        FetchEvents() {(events : [EventModel]) in
            completion(events, nil)
            }.execute(params)
        
    }
    
}
