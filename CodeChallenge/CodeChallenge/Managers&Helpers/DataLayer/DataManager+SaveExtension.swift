import Foundation
extension DataManager {
    
    //MARK: Channels Data Provider
    func saveChannelDataToRealm(_ channelsArray : [ChannelModel]) -> [ChannelModel] {
        
        let realm = RealmProvider.realm()
        
        do {
            try realm?.write {
                for channel in channelsArray{
                    realm?.add(channel)
                }
            }
        } catch  {
            return [ChannelModel]()
        }
        
        return channelsArray
    }
    
    func saveEventDataToRealm(_ eventsArray : [EventModel]) -> [EventModel] {
        
        // P.S EventModel Caching not required yet. if required uncomment
        
        /*
        let realm = RealmProvider.realm()
        
        do {
            try realm?.write {
                for event in eventsArray{
                    realm?.add(event)
                }
            }
        } catch  {
            return [EventModel]()
        }*/
        
        return eventsArray
    }
    
}
