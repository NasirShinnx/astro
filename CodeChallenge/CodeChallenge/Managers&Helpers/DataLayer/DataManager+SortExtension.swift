import Foundation

extension DataManager
{
    func sortChannelsData(_ channelsArray : [ChannelModel], type : SortType) -> [ChannelModel] {
        
        var sortedChannels : [ChannelModel]
        
        switch type {
        case .channelID:
            sortedChannels = channelsArray.sorted(by: { (first: ChannelModel, second: ChannelModel) -> Bool in
                first.channelId.value! < second.channelId.value!
            })
            
        case .channelName:
            sortedChannels = channelsArray.sorted(by: { (first: ChannelModel, second: ChannelModel) -> Bool in
                first.channelTitle < second.channelTitle
            })
        }
        
        return sortedChannels
        
    }
        
}
