/*
 
 NASIR MAHMOOD
 
 Comment:
 Work in combination with  Network manager and Response to handle types of data
 Network
 
 */
import Foundation
import Alamofire
import AlamofireObjectMapper


class FetchEvents : Command {
    
    var completionHandler: ([EventModel]) -> Void
    
    required init(completionHandler: @escaping ([EventModel])->Void) {
        self.completionHandler = completionHandler
    }
    
    public func execute(_ params : [String : Any]) {
        NetworkManager.sharedInstance
            .request("\(basicURL)\(APIEndPointType.events.rawValue)", method: .get, parameters: params)
            .responseObject { (response: DataResponse<EventList>) in
                guard let eventList = response.value else {
                    return
                }
                self.completionHandler(DataManager.sharedInstance.saveEventDataToRealm(eventList.events))
        }

    }
}
