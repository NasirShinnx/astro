/*
 
 NASIR MAHMOOD
 
 Comment:
 Work in combination with  Network manager and Response to handle types of data
 Network
 
 */
import Foundation
import Alamofire
import AlamofireObjectMapper

class FetchChannels : Command {
    
    var completionHandler: ([ChannelModel]) -> Void
    
    required init(completionHandler: @escaping ([ChannelModel])->Void) {
        self.completionHandler = completionHandler
    }
    
    public func execute(_ endpoint : APIEndPointType) {
        NetworkManager.sharedInstance
            .request("\(basicURL)\(endpoint.rawValue)", method: .get, parameters: nil)
            .responseObject { (response: DataResponse<ChannelList>) in
                guard let channelList = response.value else {
                    return
                }
                self.completionHandler(DataManager.sharedInstance.saveChannelDataToRealm(channelList.channels))
        }

    }
}
