/*
 
 NASIR MAHMOOD
 
 Comment:
 Unity
 Global Settings you want in project
 
 */

import Foundation
import UIKit

class Utility {
    
    //MARK: Keep terminal clear for useful notificaitons, uncomment if needed to check constaints
    static func clearTerminal()
    {
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
    }
    
}
