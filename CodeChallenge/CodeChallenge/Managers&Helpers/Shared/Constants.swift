/*
 
 NASIR MAHMOOD
 
 Comment:
 Constants
 All the constants will be here
 
 */

import Foundation
import UIKit

let basicURL =  "http://ams-api.astro.com.my/ams/v3"
typealias EventScrollClosure = (CGPoint) -> Void

// API endpoints
enum APIEndPointType : String {
    case channelsList              =  "/getChannelList"
    case channelDetail             =  "/getChannels"
    case events                    =  "/getEvents"
    
}

enum SortType: String {
    case channelID                = "channelId"
    case channelName              = "channelTitle"
}

internal enum Astro {
    
    enum View {
        enum Guide : Int {
            case OneHourWidth = 100
            case EventCellHeight = 99
            enum Header : Int{
                case CellHeight = 30
                case CellWidth = 100
            }
        }
    }
    
}

// Realm Environments, needed to write Unit tests
struct RealmInMemoryEnvironments {
    static let TEST                = "test"
    static let APPLICATION         = "Channels"
    
}

// Viewcontroller Titles
struct ViewControllerTitles {
    static let Channels            = "Channels"
    static let TVGuide             = "TV Guide"
    
}

// Cell Identifiers
struct CellIdentifiers {
    static let Channels            = "ChannelsTableViewCustomCell"
    static let ChannelList         = "TVGuideTableViewCustomCell"
    static let EventList           = "EventsCollectionViewCell"
    static let TimeHeaderCell      = "TVGuideHeaderCell"
    
    
}







