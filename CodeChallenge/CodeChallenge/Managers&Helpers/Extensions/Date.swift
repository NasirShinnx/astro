/*
 
 NASIR MAHMOOD
 
 Comment:
 Extension on Date OBJECT
 
 */
import Foundation

extension Date{
    func string(formatted: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatted
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: self)
    }
    
    func diffInMinutesWith(date: Date) -> Int {
        return Int(self.timeIntervalSince1970 - date.timeIntervalSince1970) / 60
    }
    
}
