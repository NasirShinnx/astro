/*
 
 NASIR MAHMOOD
 
 Comment:
 View-Controller for Channel Guide Body
 
 */

import Foundation
import UIKit
import SwiftDate

class  EventsCollectionViewController: NSObject{
    
    var collectionView : UICollectionView?
    var model: [EventModel] = [EventModel]()
    var periodStart : Date
    var eventsScrolledSignal: EventScrollClosure


    init(collectionView: UICollectionView, modelArray:[EventModel], eventsScrolledSignal: @escaping EventScrollClosure, periodStart: Date) {
        self.collectionView = collectionView
        self.eventsScrolledSignal = eventsScrolledSignal
        self.model=modelArray
        self.periodStart=periodStart
    }

    public func eventsScrolledWith(offset:CGPoint) {
        collectionView?.contentOffset = offset
    }

    public func adjustCells() {
        
        // Add an extra vacent cell in the starting to adjust the time offset
        if let event = model.first, let eventStartTime = event.eventStartTime{
            if eventStartTime > periodStart {
                let minutesDifferenceFromPeriodStart = eventStartTime.timeIntervalSince(periodStart) / 60.0
                let event = dummyEventwith(duration: minutesDifferenceFromPeriodStart)
                self.model.insert(event, at: 0)
            }
        }
        
        // add missing vacent time cells if any
        let totalEventsCount = model.count
        var index = 1
        for _ in 1..<totalEventsCount-1{
            let event = model[index]
            let nextEvent = model[index+1]
            
            if let currentEventStartTime =  event.eventStartTime, let currentEventDuration = event.displayDurationInMinutes.value, let nextEventStartTime = nextEvent.eventStartTime {
                if nextEventStartTime.diffInMinutesWith(date: currentEventStartTime) > currentEventDuration {
                    let missingTime = nextEventStartTime.diffInMinutesWith(date: currentEventStartTime) - currentEventDuration
                    let event = dummyEventwith(duration: Double(missingTime))
                    self.model.insert(event, at: index+1)
                    
                }
            }
            index = index + 1
        }
    }
    
}




extension EventsCollectionViewController: UICollectionViewDataSource{
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return model.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.EventList, for: indexPath)
        
        if let collectioncell =  cell as? EventsCollectionViewCell {
            let event  = model[indexPath.item]
            collectioncell.configure(event)
            return collectioncell
        }
        
        return cell
        
    }
}


extension EventsCollectionViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    public func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return   CGSize(width: model[indexPath.item].eventViewWidth, height: Astro.View.Guide.EventCellHeight.rawValue)
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        eventsScrolledSignal(scrollView.contentOffset)
    }

}

extension EventsCollectionViewController{
    
    func dummyEventwith(duration:Double) -> EventModel {
        
        let durationStr = String(Int(duration))
        let dummmy : [String: Any] = [ "responseCode": 200,
                                       "responseMessage": "success",
                                       "getevent": [["displayDuration":"00:\(durationStr):00"]]
        ]
        
        let eventListResponse = EventList(JSON: dummmy)!
        let events = eventListResponse.events
        let event = events.first!
        return event
        
    }
}
