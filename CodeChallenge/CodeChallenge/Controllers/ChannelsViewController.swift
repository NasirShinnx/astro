/*
 
 NASIR MAHMOOD
 
 Comment:
 Controller Object
 
 */
import UIKit
import RealmSwift
import SVProgressHUD

class ChannelsViewController: UIViewController {
    
    fileprivate var channelsArray : [ChannelModel] = []
    @IBOutlet var tableView: UITableView!

    var tableViewEstimatedHight = Float?(70.0)
    var tableViewCellIdentifier = ""
    
    let numberOfSections = 1
    var realm : Realm?
    
    //MARK: UIView Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = RealmProvider.realm()
        tableView.estimatedRowHeight = CGFloat(tableViewEstimatedHight!)
        tableView.rowHeight = UITableViewAutomaticDimension;

        title=ViewControllerTitles.Channels
        
        SVProgressHUD.show()
        DataManager.sharedInstance.fetchChannelsData(APIEndPointType.channelsList, sortType: SortType.channelName) { (channelsArrayy, error) in
            self.channelsArray=channelsArrayy!
            OperationQueue.main.addOperation {
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}

extension ChannelsViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channelsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.Channels)! as! ChannelsTableViewCustomCell
        let channel = channelsArray[indexPath.row]
        cell.configure(channel)
        cell.favourtieButton.addTarget(self, action: #selector(favourtieButtonPressed), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // segue with sender
    }
}

extension ChannelsViewController
{
    func favourtieButtonPressed(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:tableView)
        let indexpath = tableView.indexPathForRow(at: buttonPosition)
        let channel = channelsArray[(indexpath?.row)!]
        do{
            var newVal = false
            if sender.isSelected {
                sender.isSelected=false
                newVal = false
            }
            else
            {
                newVal = true
                sender.isSelected=true
            }
            
            try realm?.write {
                channel.favorite.value = newVal
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        catch{
            print("error writing object")
        }
    }

}
//MARK: Sorting Extenion, DataManager+SortExtension helps here
extension ChannelsViewController
{
    @IBAction func sort1to9ItemPressed(_ sender : AnyObject){
        channelsArray=DataManager.sharedInstance.sortChannelsData(channelsArray, type: .channelID)
        tableView.reloadData()
    }
    
    @IBAction func sortAtoZItemPressed(_ sender : AnyObject){
        channelsArray=DataManager.sharedInstance.sortChannelsData(channelsArray, type: .channelName)
        tableView.reloadData()
    }
}

