/*
 
 NASIR MAHMOOD
 
 Comment:
 Controller Object
 
 */
import Foundation
import UIKit
import RealmSwift
import SwiftDate
import SVProgressHUD

class TVGuideViewController: UIViewController {
    
    var channelsListArray : [ChannelModel] = []
    var currentOffset : CGPoint = CGPoint(x: 0, y: 0)
    var timeHeaderCollectionViewController : TVGuideHeaderCollectionViewController!
    
    @IBOutlet var tableView : UITableView!
    @IBOutlet var timeHeaderCollectionView: UICollectionView!

    @IBOutlet var leftArrowButton  : UIButton!
    @IBOutlet var rightArrowButton : UIButton!
    @IBOutlet var currentDateLabel : UILabel!
    
    let numberOfSections = 1
    var periodStart = Date().startOf(component: .day)
    var periodEnd = Date().endOf(component: .day)

    
    //MARK: UIView Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title=ViewControllerTitles.TVGuide
        configreHeader()
        
        SVProgressHUD.show()
        currentDateLabel.text = periodStart.string(custom: "EEEE yyyy-MM-dd")
        DataManager.sharedInstance.fetchChannelsData(APIEndPointType.channelsList, sortType: SortType.channelName) { (channelsArrayy, error) in
            self.channelsListArray=channelsArrayy!
            OperationQueue.main.addOperation {
                self.moveGuideToCurrentDateTime()
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configreHeader() -> Void  {
        timeHeaderCollectionViewController = TVGuideHeaderCollectionViewController(collectionView: timeHeaderCollectionView, offset:currentOffset)
        self.timeHeaderCollectionView.contentOffset=currentOffset
        timeHeaderCollectionView.delegate = timeHeaderCollectionViewController
        timeHeaderCollectionView.dataSource = timeHeaderCollectionViewController
    }
    
    // Move the collectionview to current time of device
    func moveGuideToCurrentDateTime() {
        eventsScrolledWith(offset: CGPoint(x: Date().hour * Astro.View.Guide.OneHourWidth.rawValue, y: 0))
    }
}

//MARK: Main TableView

extension TVGuideViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channelsListArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.ChannelList)! as! TVGuideTableViewCustomCell
        let channel = channelsListArray[indexPath.row]
        cell.configure(channel, startDate: periodStart, endDate: periodEnd, eventsScrolledSignal:self.eventsScrolledWith, initialOffset: self.currentOffset)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // segue with sender
    }
    
    func tableEventsScrolledWith(offset:CGPoint) {
        self.currentOffset = offset
        for cell in tableView.visibleCells{
            if let guideCell = cell as? TVGuideTableViewCustomCell{
                guideCell.eventsScrolledWith(offset: offset)
            }
        }
        
    }
}

//MARK: Sorting Extenion
extension TVGuideViewController
{
    @IBAction func sort1to9ItemPressed(_ sender : AnyObject){
        channelsListArray=DataManager.sharedInstance.sortChannelsData(channelsListArray, type: .channelID)
        tableView.reloadData()
    }
    
    @IBAction func sortAtoZItemPressed(_ sender : AnyObject){
        channelsListArray=DataManager.sharedInstance.sortChannelsData(channelsListArray, type: .channelName)
        tableView.reloadData()
    }
}
//MARK: Fetching Next and Previous Data
extension TVGuideViewController
{
    @IBAction func leftArrowButtonPressed(_ sender : AnyObject) {
        periodStart = (periodStart - 1.day).startOfDay
        periodEnd   = (periodEnd - 1.day).endOfDay
        currentDateLabel.text = periodStart.string(custom: "EEEE yyyy-MM-dd")
        tableView.reloadData()
    }
    
    @IBAction func rightArrowButtonPressed(_ sender : AnyObject) {
        periodStart = (periodStart + 1.day).startOfDay
        periodEnd   = (periodEnd + 1.day).endOfDay
        currentDateLabel.text = periodStart.string(custom: "EEEE yyyy-MM-dd")
        tableView.reloadData()
    }
}

// MARK: Scroll
extension TVGuideViewController : UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != tableView {
            currentOffset = scrollView.contentOffset
            eventsScrolledWith(offset: scrollView.contentOffset)
        }
        
    }
    
    func eventsScrolledWith(offset:CGPoint) {
        timeHeaderCollectionViewController.eventsScrolledWith(offset: offset)
        tableEventsScrolledWith(offset: offset)
    }
}


