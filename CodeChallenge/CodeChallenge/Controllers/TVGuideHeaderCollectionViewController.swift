/*
 
 NASIR MAHMOOD
 
 Comment:
 View-Controller for Channel Guide Header
 
 */

import Foundation
import UIKit
import SwiftDate
class  TVGuideHeaderCollectionViewController: NSObject{
    
    var collectionView: UICollectionView
    var model: [Date] = [Date]()

    init(collectionView: UICollectionView, offset: CGPoint) {
        self.collectionView = collectionView
        self.collectionView.contentOffset=offset
        let startOfDay = Date().startOfDay
        model.append(startOfDay)

        var j: Int = 0
        for _ in 1..<24 {
            j = j + 1
            model.append(startOfDay + j.hour)
        }
    }   
    
    func eventsScrolledWith(offset:CGPoint) {
        collectionView.contentOffset = offset
    }
}

extension TVGuideHeaderCollectionViewController: UICollectionViewDataSource{
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return model.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.TimeHeaderCell, for: indexPath)
        
        if let headerCell =  cell as? TVGuideHeaderCell {
            headerCell.timeLabel.text = model[indexPath.item].string(formatted: "hh a")
            return headerCell
        }
        
        return cell
        
    }
}

extension TVGuideHeaderCollectionViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return   CGSize(width: Astro.View.Guide.Header.CellWidth.rawValue, height: Astro.View.Guide.Header.CellHeight.rawValue)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}
