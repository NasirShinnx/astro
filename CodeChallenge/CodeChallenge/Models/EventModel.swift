/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 Made with Realm and Object MApper to automap objects to RealmDatabase.
 
 */

import Foundation
import RealmSwift
import ObjectMapper

fileprivate struct EventJsonConstants{

}


class EventModel: Object, Mappable {

    dynamic var eventID            = ""
    dynamic var channelStbNumber   = ""
    dynamic var channelHD          = ""
    dynamic var channelTitle       = ""
    dynamic var epgEventImage      = ""
    dynamic var certification      = ""
    dynamic var displayDateTimeUtc = ""
    dynamic var displayDateTime    = ""
    dynamic var displayDuration    = ""
    dynamic var siTrafficKey       = ""
    dynamic var programmeTitle     = ""
    dynamic var programmeId        = ""
    dynamic var episodeId          = ""
    dynamic var shortSynopsis      = ""
    dynamic var longSynopsis       = ""
    dynamic var actors             = ""
    dynamic var directors          = ""
    dynamic var producers          = ""
    dynamic var genre              = ""
    dynamic var subGenre           = ""
    dynamic var highlight          = ""
    dynamic var contentId          = ""
    dynamic var contentImage       = ""
    
    var channelId   =  RealmOptional<Int>()
    var live        =  RealmOptional<Bool>()
    var premier     =  RealmOptional<Bool>()
    var ottBlackout =  RealmOptional<Bool>()
    var groupKey    =  RealmOptional<Int>()
    
    var eventStartTime   : Date?
    var displayDurationInMinutes =  RealmOptional<Int>()
    var eventViewWidth : Int{
        get{
            if  displayDurationInMinutes.value != nil {
                let width = (Double(displayDurationInMinutes.value!) / 60.0 ) * Double(Astro.View.Guide.OneHourWidth.rawValue)
                return Int(width)
            }
            return 0
        }
    }
    
    override static func primaryKey() -> String? {
        return "eventID"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
}

//MARK: Mapper Extension
extension EventModel
{
    func mapping(map: Map) {
        eventID            <- map["eventID"]
        channelId.value    <- map["channelId"]
        channelStbNumber   <- map["channelStbNumber"]
        channelHD          <- map["channelHD"]
        channelTitle       <- map["channelTitle"]
        epgEventImage      <- map["epgEventImage"]
        certification      <- map["certification"]
        displayDateTimeUtc <- map["displayDateTimeUtc"]
        displayDateTime    <- map["displayDateTime"]
        displayDuration    <- map["displayDuration"]
        siTrafficKey       <- map["siTrafficKey"]
        programmeTitle     <- map["programmeTitle"]
        programmeId        <- map["programmeId"]
        episodeId          <- map["episodeId"]
        shortSynopsis      <- map["shortSynopsis"]
        longSynopsis       <- map["longSynopsis"]
        actors             <- map["actors"]
        directors          <- map["directors"]
        producers          <- map["producers"]
        genre              <- map["genre"]
        subGenre           <- map["subGenre"]
        live.value         <- map["live"]
        premier.value      <- map["premier"]
        ottBlackout.value  <- map["ottBlackout"]
        highlight          <- map["highlight"]
        contentId          <- map["contentId"]
        contentImage       <- map["contentImage"]
        groupKey.value     <- map["groupKey"]
        
        eventStartTime     <- (map["displayDateTimeUtc"], UTCDateTransformer())
        displayDurationInMinutes.value <-  (map["displayDuration"], DisplayTimeTransformer())
    }

}
