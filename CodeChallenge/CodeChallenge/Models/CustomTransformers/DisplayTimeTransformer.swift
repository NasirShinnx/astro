/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 
 */
import Foundation
import ObjectMapper

open class DisplayTimeTransformer: TransformType {
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> Int? {

        var totalTime = 0
        if let diaplayTimeStr = value as? String {
            let components = diaplayTimeStr.components(separatedBy: ":")
            if let hourComponent = components.first, let hours = Int(hourComponent){
                totalTime = totalTime + hours*60
            }
            if components.count > 1{
                let minComponent = components[1]
                if let minutes = Int(minComponent){
                    totalTime = totalTime + minutes
                }
            
            }
            if let secComponent = components.last, let secs = Int(secComponent){
                totalTime = totalTime + (secs/60)
            }
        }
        
        return totalTime
    }
    
    open func transformToJSON(_ value: Int?) -> Int? {
        if let value = value {
            return value
        }
        return nil
    }
    
}
