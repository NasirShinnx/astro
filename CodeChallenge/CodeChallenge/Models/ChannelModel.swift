/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 Made with Realm and Object MApper to automap objects to RealmDatabase.
 
 */

import Foundation
import RealmSwift
import ObjectMapper

fileprivate struct JSONConstants {
    static let kChannelID            = "channelId"
    static let kChannelName          = "channelTitle"
    static let kChannelStbNumber     = "channelStbNumber"
    static let kFavourite            = "favorite"
    
}

class ChannelModel: Object, Mappable {

    dynamic var channelTitle =  ""
    let favorite = RealmOptional<Bool>()
    let channelId = RealmOptional<Int>()
    let channelStbNumber = RealmOptional<Int>()

    override static func primaryKey() -> String? {
        return JSONConstants.kChannelID
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
}

//MARK: Mapper Extension
extension ChannelModel
{
    func mapping(map: Map) {
        channelId.value <- map[JSONConstants.kChannelID]
        channelTitle <- map[JSONConstants.kChannelName]
        channelStbNumber.value <- map[JSONConstants.kChannelStbNumber]
    }

}

enum DataError: Error {
    case missing(String)
    case invalid(String, Any)
}
