/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 Made with Realm and Object MApper to automap objects to RealmDatabase.
 
 */

import Foundation
import ObjectMapper

fileprivate struct JSONConstants {
    static let kEvents            = "getevent"
}

class EventList: Mappable {
    var events = [EventModel]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        events <- map[JSONConstants.kEvents]
    }
}
