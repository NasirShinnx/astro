/*
 
 NASIR MAHMOOD
 
 Comment:
 MODEL OBJECT
 Made with Realm and Object MApper to automap objects to RealmDatabase.
 
 */

import Foundation
import ObjectMapper

fileprivate struct JSONConstants {
    static let kChannels            = "channels"
}

class ChannelList: Mappable {
    var channels = [ChannelModel]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        channels <- map[JSONConstants.kChannels]
    }
}
