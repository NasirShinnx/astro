import Foundation
import UIKit

class EventsCollectionViewCell: UICollectionViewCell  {
    
    @IBOutlet var eventNameLabel   : UILabel!
    @IBOutlet var eventTimeLabel   : UILabel!
    
    open func configure(_ event : EventModel)  {
        eventNameLabel.text = "\(event.programmeTitle)"
        if let programmeTime = event.eventStartTime{
            eventTimeLabel.text = "\(programmeTime.string(formatted: "hh:mm a") ?? "")"
        }
        else{
            eventTimeLabel.text = ""
        }
       
    }
}
