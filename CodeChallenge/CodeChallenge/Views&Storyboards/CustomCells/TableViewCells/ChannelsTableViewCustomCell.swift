import Foundation
import UIKit

class ChannelsTableViewCustomCell: UITableViewCell {
    
    @IBOutlet var channelNoLabel   : UILabel!
    @IBOutlet var channelNameLabel : UILabel!
    @IBOutlet var favourtieButton  : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    open func configure(_ channelObj : ChannelModel)  {
        channelNoLabel.text = "\((channelObj.channelId.value)!)"
        channelNameLabel.text = "\(channelObj.channelTitle)"
        favourtieButton.isSelected=false
        if let favourite = channelObj.favorite.value {
            favourtieButton.isSelected=favourite
        }

    }
    
}
