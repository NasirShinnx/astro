import Foundation
import UIKit
import SVProgressHUD

class TVGuideTableViewCustomCell: UITableViewCell {
    
    @IBOutlet var channelNameLabel : UILabel!
    @IBOutlet var collectionView   : UICollectionView!
    var eventsCollectionViewController : EventsCollectionViewController?

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    open func configure(_ channelObj : ChannelModel, startDate:Date, endDate:Date, eventsScrolledSignal: @escaping EventScrollClosure, initialOffset: CGPoint)  {
        channelNameLabel.text = "\(channelObj.channelTitle)"
        fetchEventDataForChannel(channelObj, startDate: startDate, endDate: endDate, eventsScrolledSignal: eventsScrolledSignal, initialOffset: initialOffset)
    }
    
    func eventsScrolledWith(offset:CGPoint) {
        if collectionView.contentOffset != offset {
            collectionView.contentOffset = offset
        }
        
        
    }
    
    private func fetchEventDataForChannel(_ channelObj : ChannelModel,  startDate:Date, endDate:Date, eventsScrolledSignal: @escaping EventScrollClosure, initialOffset: CGPoint){
        
        SVProgressHUD.show()
        DataManager.sharedInstance.fetchEventsParamsForChannel(channelObj, periodStart: startDate, periodEnd: endDate) { (array, error) in
            self.eventsCollectionViewController = EventsCollectionViewController(collectionView: self.collectionView, modelArray:array!, eventsScrolledSignal:eventsScrolledSignal, periodStart: startDate)
            
                self.eventsCollectionViewController?.adjustCells()
                self.collectionView.dataSource=self.eventsCollectionViewController
                self.collectionView.delegate=self.eventsCollectionViewController
                self.collectionView.contentOffset = initialOffset

            
                OperationQueue.main.addOperation {
                    SVProgressHUD.dismiss()
                    self.collectionView.reloadData()
                }

            
        }
        
    }
    
}
/*
if collectionView.contentOffset != offset {
    let oldOffset = collectionView.contentOffset
    collectionView.contentOffset=CGPoint.init(x: offset.x, y: oldOffset.y)
}*/
